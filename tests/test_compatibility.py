# mdx_smidgens -- Python Markdown extension to include pre-made snippets.
# Copyright (C) 2019-2024 Daniel Fancsali <fancsali@gmail.com>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License version 3
# as published by the Free Software Foundation

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import unittest
import markdown
from mdx_smidgens import SmidgensExtension
import os


class TestCompatibility(unittest.TestCase):
    def setUp(self):
        snippet_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)), "files")
        smidgen_extension = SmidgensExtension(path=snippet_dir)
        self.md = markdown.Markdown(extensions=[smidgen_extension, 'markdown.extensions.extra'])

    def test_attr_list_works(self):
        """Test that the attribute list extension is loaded"""
        md_input = "One line\n{: .one }"
        exprected = "<p class=\"one\">One line</p>"
        result = self.md.convert(md_input)

    def test_it_works_with_attr_list(self):
        """Test compatibility with the built-in attribute list extension"""
        md_input = "@@@ attr_list Argument List\n"
        expected = "<p>Argument List</p>\n<p class=\"foo\">Paragraph</p>"
        result = self.md.convert(md_input)
        self.assertEqual(expected, result)

    def test_markdown_in_html(self):
        """It shall be usable with the extras plugin and markdown=1 within HTML"""
        md_input = "@@@ div Spam\n    Some text.\n\nThe rest!\n"
        expected = "<div>\n<h1>Spam</h1>\n<p>Some text.</p>\n</div>\n<p>The rest!</p>"
        result = self.md.convert(md_input)
        self.assertEqual(expected, result)

if __name__ == '__main__':
        unittest.main()
