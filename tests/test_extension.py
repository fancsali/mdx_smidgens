# mdx_smidgens -- Python Markdown extension to include pre-made snippets.
# Copyright (C) 2019-2024 Daniel Fancsali <fancsali@gmail.com>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License version 3
# as published by the Free Software Foundation

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import unittest
import markdown
from mdx_smidgens import SmidgensExtension
import os


class TestExtension(unittest.TestCase):
    def setUp(self):
        snippet_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)), "files")
        smidgen_extension = SmidgensExtension(path=snippet_dir)
        self.md = markdown.Markdown(extensions=[smidgen_extension])

    def test_it_finds_smidgen_name(self):
        """Extracts the name of the required smidgen correctly"""
        md_input = "@@@ null\n"
        expected = "<p>This is the NULL partial!</p>"
        result = self.md.convert(md_input)
        self.assertEqual(expected, result)

    def test_it_handles_missing_name(self):
        """It will ignore instances where name is missing"""
        md_input = "@@@ \n"
        expected = "<p>@@@ </p>"
        result = self.md.convert(md_input)
        self.assertEqual(expected, result)

    def test_finds_partials_in_subdirectories(self):
        md_input = "@@@ subdir/partial\n"
        expected = "<p>This comes from a subdirectory!</p>"
        result = self.md.convert(md_input)
        self.assertEqual(expected, result)

    def test_finds_partials_with_dash(self):
        md_input = "@@@ partial-dash\n"
        expected = "<p>This has a dash in the name!</p>"
        result = self.md.convert(md_input)
        self.assertEqual(expected, result)

    def test_it_collects_arguments(self):
        """It will parse the 'title'-line for arguments correctly"""
        md_input = "@@@ echo2 Arg1 Arg2\n"
        expected = "<p>This is expecting two arguments!\nFirst: Arg1\nSecond: Arg2</p>"
        result = self.md.convert(md_input)
        self.assertEqual(expected, result)

    def test_it_understands_quoted_arguments(self):
        """It will parse quoted arguments"""
        md_input = "@@@ echo2 \"Argument One\" Arg2\n"
        expected = "<p>This is expecting two arguments!\nFirst: Argument One\nSecond: Arg2</p>"
        result = self.md.convert(md_input)
        self.assertEqual(expected, result)

    def test_missing_arguments_are_considered_empty(self):
        """It will replace missing arguments with empty strings"""
        md_input = "@@@ echo2 Arg1\n"
        expected = "<p>This is expecting two arguments!\nFirst: Arg1\nSecond: </p>"
        result = self.md.convert(md_input)
        self.assertEqual(expected, result)

    def test_it_injects_whole_argument_line(self):
        """It will provide {0} to inject the whole argument list"""
        md_input = "@@@ echo0 Arg1 Arg2\n"
        expected = "<p>This is the whole argument line: Arg1 Arg2</p>"
        result = self.md.convert(md_input)
        self.assertEqual(expected, result)

    def test_it_extracts_blocks(self):
        """It shall find indented blocks"""
        md_input = "@@@ echo_args_content Spam Eggs\n    First line\n    And second line\n"
        expected = "<p>Partial for args and content!\nFirst argument: Spam\nSecond argument: Eggs\nFirst line\nAnd second line</p>\n<hr />"
        result = self.md.convert(md_input)
        self.assertEqual(expected, result)

    def test_it_finds_all_blocks(self):
        """It shall find all consecutive indented blocks"""
        md_input = "@@@ echo_args_content Spam Eggs\n    First line\n    And second line\n\n    Another block!"
        expected = "<p>Partial for args and content!\nFirst argument: Spam\nSecond argument: Eggs\nFirst line\nAnd second line</p>\n<p>Another block!</p>\n<hr />"
        result = self.md.convert(md_input)
        self.assertEqual(expected, result)

    def test_it_will_not_swallow_next_block(self):
        """It shall not touch the non-indented blocks that follow"""
        md_input = "@@@ echo_args_content Spam Eggs\n    First line\n    And second line\n\n    Another block!\n\nUnindented!"
        expected = "<p>Partial for args and content!\nFirst argument: Spam\nSecond argument: Eggs\nFirst line\nAnd second line</p>\n<p>Another block!</p>\n<hr />\n\n<p>Unindented!</p>"
        result = self.md.convert(md_input)
        self.assertEqual(expected, result)

    def test_it_will_not_break_inline_markdown(self):
        """It shall run the generated text through the inline MD parsers"""
        md_input = "@@@ echo_content Spam Eggs\n    Some **BOLD** and *italic*.\n\n"
        expected = "<p>Partial for content only!\nSome <strong>BOLD</strong> and <em>italic</em>.</p>\n<hr />"
        result = self.md.convert(md_input)
        self.assertEqual(expected, result)

    def test_it_will_not_break_block_markdown(self):
        """It shall run the generated text through the blockprocessors"""
        md_input = "@@@ echo_block_content Spam Eggs\n    - Item 1\n    - Item 2\n\n    - Another item\n"
        expected = "<p>Blocks...</p>\n<ul>\n<li>Item 1</li>\n<li>\n<p>Item 2</p>\n</li>\n<li>\n<p>Another item</p>\n</li>\n</ul>\n<hr />"
        result = self.md.convert(md_input)
        self.assertEqual(expected, result)

    def test_recursion_block_block(self):
        """It should work when a block is invoked recursively within a block"""
        md_input = "@@@ echo_block_content Spam Eggs\n    @@@ echo_block_content\n        Inner content\n"
        expected = "<p>Blocks...</p>\n<p>Blocks...</p>\n<p>Inner content</p>\n<hr />\n\n<hr />"
        result = self.md.convert(md_input)
        self.assertEqual(expected, result)

    def test_recursion_block_inline(self):
        """It should work when invoked inline within a block"""
        md_input = "@@@ echo_block_content Spam Eggs\n    {@ inline @}\n"
        expected = "<p>Blocks...</p>\n<p>NOARGS</p>\n<hr />"
        result = self.md.convert(md_input)
        self.assertEqual(expected, result)

    @unittest.skip("Regular expressions cannot handle nesting; this is undefined!")
    def test_recursion_inline_inline(self):
        """It should work when invoked inline within an inline tag"""
        md_input = "{@ inline2 {@ inline @} {inline @} @}\n"
        expected = "<p>args: NOARGS / NOARGS</p>"
        result = self.md.convert(md_input)
        self.assertEqual(expected, result)

    @unittest.skip("Conceptually ambiguous; so undefined!")
    def test_recursion_inline_block(self):
        """Invoking a block within an inline tag should work"""
        md_input = "{@ inline2 BLOCK @@@ null @}\n"
        expected = "???"
        result = self.md.convert(md_input)
        self.assertEqual(expected, result)


    def test_escape_block(self):
        """It should ignore escaped block invocations"""
        md_input = "\@@@ echo_block_content Spam Eggs\n    {@ inline @}\nHam!"
        expected = "<p>@@@ echo_block_content Spam Eggs\n    NOARGS\nHam!</p>"
        result = self.md.convert(md_input)
        self.assertEqual(expected, result)

    def test_escape_inline(self):
        """It should ingore escaped inline invocations"""
        md_input = "\{@ inline @} {\@ inline @} \{\@ inline @}"
        expected = "<p>{@ inline @} {@ inline @} {@ inline @}</p>"
        result = self.md.convert(md_input)
        self.assertEqual(expected, result)


if __name__ == '__main__':
        unittest.main()
