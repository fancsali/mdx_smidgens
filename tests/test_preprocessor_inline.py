# mdx_smidgens -- Python Markdown extension to include pre-made snippets.
# Copyright (C) 2019-2024 Daniel Fancsali <fancsali@gmail.com>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License version 3
# as published by the Free Software Foundation

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import unittest
from mdx_smidgens import SmidgensPreprocessor
import markdown
import os


class TestPreprocessorInline(unittest.TestCase):

    def setUp(self):
        config = {}
        config['path'] = os.path.join(os.path.dirname(os.path.realpath(__file__)), "files")
        config['prefix'] = '@'
        config['recursion_max'] = 9
        md = markdown.Markdown()
        self.preprocessor = SmidgensPreprocessor(md, config)

    def test_finds_name(self):
        """Extracts the name of the smidgen"""
        lines = ["AAA", "{@ null @}", "ZZZ"]
        expected = ["AAA", "This is the NULL partial!", "ZZZ"]
        result = self.preprocessor.run(lines)
        self.assertEqual(expected, result)

    def test_ignores_missing_name(self):
        """Ignore the ones with missing name"""
        lines = ["AAA", "{@ @}", "ZZZ"]
        expected = ["AAA", "{@ @}", "ZZZ"]
        result = self.preprocessor.run(lines)
        self.assertEqual(expected, result)

    def test_finds_in_subdirectories(self):
        """It will find files in subdirectories"""
        lines = ["AAA", "{@ subdir/partial @}", "ZZZ"]
        expected = ["AAA", "This comes from a subdirectory!", "ZZZ"]
        result = self.preprocessor.run(lines)
        self.assertEqual(expected, result)

    def test_dashed_name(self):
        """It will find files with dashes in their name"""
        lines = ["AAA", "{@ partial-dash @}", "ZZZ"]
        expected = ["AAA", "This has a dash in the name!", "ZZZ"]
        result = self.preprocessor.run(lines)
        self.assertEqual(expected, result)

    def test_line_beginning(self):
        """It should work at the very beginning of a line!"""
        lines = ["AAA", "{@inline@} BAR", "ZZZ"]
        expected = ["AAA", "NOARGS BAR", "ZZZ"]
        result = self.preprocessor.run(lines)
        self.assertEqual(expected, result)

    def test_line_end(self):
        """It should work at the very end of a line!"""
        lines = ["AAA", "FOO {@inline@}", "ZZZ"]
        expected = ["AAA", "FOO NOARGS", "ZZZ"]
        result = self.preprocessor.run(lines)
        self.assertEqual(expected, result)

    def test_line_middle(self):
        """It should work in the middle of a line!"""
        lines = ["AAA", "FOO {@inline@} BAR", "ZZZ"]
        expected = ["AAA", "FOO NOARGS BAR", "ZZZ"]
        result = self.preprocessor.run(lines)
        self.assertEqual(expected, result)

    def test_word_middle(self):
        """It should work in the middle of a word!"""
        lines = ["AAA", "foo{@inline@}bar", "ZZZ"]
        expected = ["AAA", "fooNOARGSbar", "ZZZ"]
        result = self.preprocessor.run(lines)
        self.assertEqual(expected, result)

    def test_single(self):
        """Replaces one on a line"""
        lines = ["AAA", "FOO {@ inline0 XXX YYY @} BAR", "ZZZ"]
        expected = ["AAA", "FOO all: XXX YYY BAR", "ZZZ"]
        result = self.preprocessor.run(lines)
        self.assertEqual(expected, result)

    def test_sameline(self):
        """Replaces more than one times on a line"""
        lines = ["AAA", "FOO {@ inline0 XXX YYY @} BAR {@ inline0 CCC @} BAZ", "ZZZ"]
        expected = ["AAA", "FOO all: XXX YYY BAR all: CCC BAZ", "ZZZ"]
        result = self.preprocessor.run(lines)
        self.assertEqual(expected, result)

    def test_quoted_arguments(self):
        """Understands quoted arguments"""
        lines = ["AAA", "XXX {@ inline2 \"FOO BAR\" BAZ @} YYY", "ZZZ"]
        expected = ["AAA", "XXX args: FOO BAR / BAZ YYY", "ZZZ"]
        result = self.preprocessor.run(lines)
        self.assertEqual(expected, result)

    def test_empty_arguments(self):
        """Empty arguments will be understood as empty strings"""
        lines = ["AAA", "@@@ inline2 FOO", "ZZZ"]
        expected = ["AAA", "args: FOO / ", "ZZZ"]

    def test_noargs(self):
        """Processes one inline without arguments"""
        lines = ["AAA", "FOO {@ inline @} BAR", "ZZZ"]
        expected = ["AAA", "FOO NOARGS BAR", "ZZZ"]
        result = self.preprocessor.run(lines)
        self.assertEqual(expected, result)

    def test_noargs_nospace(self):
        """Processes one inline without arguments or spaces"""
        lines = ["AAA", "FOO {@inline@} BAR", "ZZZ"]
        expected = ["AAA", "FOO NOARGS BAR", "ZZZ"]
        result = self.preprocessor.run(lines)
        self.assertEqual(expected, result)

    def test_empty_lines(self):
        """It should not touch empty lines"""
        lines = ["AAA", "  ", "", "ZZZ"]
        expected = ["AAA", "  ", "", "ZZZ"]
        result = self.preprocessor.run(lines)
        self.assertEqual(expected, result)

    def test_recursion_in_template(self):
        """It should work when invoked recursively"""
        lines = ["AAA", "SPAM {@ recursion_inline @} EGGS", "ZZZ"]
        expected = ["AAA", "SPAM FOO NOARGS BAR EGGS", "ZZZ"]
        result = self.preprocessor.run(lines)
        self.assertEqual(expected, result)

    def test_recursion_limit(self):
        """It should not get stuck"""
        lines = ["AAA", "A {@infinite_inline @}", "ZZZ"]
        expected = ["AAA", "A XXXXXXXXX{@ infinite_inline @}", "ZZZ"]
        result = self.preprocessor.run(lines)
        self.assertEqual(expected, result)

    def test_recursion_limit_config(self):
        """Recursion limit can be changed"""
        config = {}
        config['path'] = os.path.join(os.path.dirname(os.path.realpath(__file__)), "files")
        config['prefix'] = '@'
        config['recursion_max'] = 3
        md = markdown.Markdown()
        other_preprocessor = SmidgensPreprocessor(md, config)

        lines = ["AAA", "A {@infinite_inline @}", "ZZZ"]
        expected = ["AAA", "A XXX{@ infinite_inline @}", "ZZZ"]
        result = other_preprocessor.run(lines)
        self.assertEqual(expected, result)


if __name__ == '__main__':
        unittest.main()
