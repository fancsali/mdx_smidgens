# mdx_smidgens -- Python Markdown extension to include pre-made snippets.
# Copyright (C) 2019-2024 Daniel Fancsali <fancsali@gmail.com>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License version 3
# as published by the Free Software Foundation

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import unittest
from mdx_smidgens import SmidgensPreprocessor
import markdown
import os


class TestPreprocessorBlock(unittest.TestCase):

    def setUp(self):
        config = {}
        config['path'] = os.path.join(os.path.dirname(os.path.realpath(__file__)), "files")
        config['prefix'] = '@'
        config['recursion_max'] = 9
        md = markdown.Markdown()
        self.preprocessor = SmidgensPreprocessor(md, config)

    def test_finds_name(self):
        """Extracts the name of the smidgen"""
        lines = ["AAA", "@@@ null", "ZZZ"]
        expected = ["AAA", "This is the NULL partial!", "ZZZ"]
        result = self.preprocessor.run(lines)
        self.assertEqual(expected, result)

    def test_ignores_missing_name(self):
        """Ignore the ones with a missing name"""
        lines = ["AAA", "@@@ ", "ZZZ"]
        expected = ["AAA", "@@@ ", "ZZZ"]
        result = self.preprocessor.run(lines)
        self.assertEqual(expected, result)

    def test_finds_in_subdirectories(self):
        """It will find files in subdirectories"""
        lines = ["AAA", "@@@ subdir/partial", "ZZZ"]
        expected = ["AAA", "This comes from a subdirectory!", "ZZZ"]
        result = self.preprocessor.run(lines)
        self.assertEqual(expected, result)

    def test_dashed_name(self):
        """It will find files in subdirectories"""
        lines = ["AAA", "@@@ partial-dash", "ZZZ"]
        expected = ["AAA", "This has a dash in the name!", "ZZZ"]
        result = self.preprocessor.run(lines)
        self.assertEqual(expected, result)

    def test_single_line(self):
        """Replaces a sinlge block statement with snippet of one line"""
        lines = ["AAA", "@@@ echo0 FOO BAR", "ZZZ"]
        expected = ["AAA", "This is the whole argument line: FOO BAR", "ZZZ"]
        result = self.preprocessor.run(lines)
        self.assertEqual(expected, result)

    def test_multiple_lines(self):
        """Replaces a sinlge block statement with snippet containing multiple lines"""
        lines = ["AAA", "@@@ echo2 FOO BAR", "ZZZ"]
        expected = ["AAA", "This is expecting two arguments!", "First: FOO" , "Second: BAR", "ZZZ"]
        result = self.preprocessor.run(lines)
        self.assertEqual(expected, result)

    def test_quoted_arguments(self):
        """Understands quoted arguments"""
        lines = ["AAA", "@@@ echo2 \"FOO BAR\" BAZ", "ZZZ"]
        expected = ["AAA", "This is expecting two arguments!", "First: FOO BAR" , "Second: BAZ", "ZZZ"]
        result = self.preprocessor.run(lines)
        self.assertEqual(expected, result)

    def test_empty_arguments(self):
        """Empty arguments will be understood as empty strings"""
        lines = ["AAA", "@@@ echo2 FOO", "ZZZ"]
        expected = ["AAA", "This is expecting two arguments!", "First: FOO" , "Second: ", "ZZZ"]

    def test_whole_arg_line(self):
        """The whole argument line is injected as arg 0"""
        lines = ["AAA", "@@@ echo0 FOO BAR", "ZZZ"]
        expected = ["AAA", "This is the whole argument line: FOO BAR", "ZZZ"]

    def test_with_content(self):
        """Processes a block statement with tabbed blocks following"""
        lines = ["AAA", "@@@ echo_content", "    FOO BAR", "ZZZ"]
        expected = ["AAA", "Partial for content only!", "FOO BAR", "", "<hr />", "ZZZ"]
        result = self.preprocessor.run(lines)
        self.assertEqual(expected, result)

    def test_multiline_content(self):
        """Processes a block statement with tabbed blocks following"""
        lines = ["AAA", "@@@ echo_content", "    FOO BAR", "    SPAM EGGS", "ZZZ"]
        expected = ["AAA", "Partial for content only!", "FOO BAR", "SPAM EGGS", "", "<hr />", "ZZZ"]
        result = self.preprocessor.run(lines)
        self.assertEqual(expected, result)

    def test_with_content_nospace(self):
        """Processes a block statement with tabbed blocks following"""
        lines = ["AAA", "@@@echo_content", "    FOO BAR", "ZZZ"]
        expected = ["AAA", "Partial for content only!", "FOO BAR", "", "<hr />", "ZZZ"]
        result = self.preprocessor.run(lines)
        self.assertEqual(expected, result)

    def test_with_more_content(self):
        """It should find multiple blocks separated by empty lines"""
        lines = ["AAA", "@@@ echo_content", "    FOO BAR", "  ", "", "    SPAM EGGS", "ZZZ"]
        expected = ["AAA", "Partial for content only!", "FOO BAR", "  ", "", "SPAM EGGS", "", "<hr />", "ZZZ"]
        result = self.preprocessor.run(lines)
        self.assertEqual(expected, result)

    def test_ignore_trailing_empty_lines(self):
        """It should not swallow empty lines, if there's no tabbed block following"""
        lines = ["AAA", "@@@ echo_content", "    FOO BAR", "  ", "", "ZZZ"]
        expected = ["AAA", "Partial for content only!", "FOO BAR", "", "<hr />", "  ", "", "ZZZ"]
        result = self.preprocessor.run(lines)
        self.assertEqual(expected, result)

    def test_empty_lines(self):
        """It should not touch empty lines"""
        lines = ["AAA", "  ", "", "ZZZ"]
        expected = ["AAA", "  ", "", "ZZZ"]
        result = self.preprocessor.run(lines)
        self.assertEqual(expected, result)

    def test_recursion_in_template(self):
        """It should work when invoked recursively"""
        lines = ["AAA", "@@@ recursion_block", "ZZZ"]
        expected = ["AAA", "This is the NULL partial!", "ZZZ"]
        result = self.preprocessor.run(lines)
        self.assertEqual(expected, result)

    def test_escaped_recursion_in_template(self):
        """It should ignore escaped triplets"""
        lines = ["AAA", "\@@@ recursion_block", "ZZZ"]
        expected = ["AAA", "\@@@ recursion_block", "ZZZ"]
        result = self.preprocessor.run(lines)
        self.assertEqual(expected, result)

    def test_recursion_in_content_block(self):
        """It should work when invoked recursively"""
        lines = ["AAA", "@@@ echo_content", "    @@@ recursion_block", "ZZZ"]
        expected = ["AAA", "Partial for content only!", "This is the NULL partial!", "", "<hr />", "ZZZ"]
        result = self.preprocessor.run(lines)
        self.assertEqual(expected, result)

    def test_recursion_in_content_inline(self):
        """It should work when invoked recursively"""
        lines = ["AAA", "@@@ echo_content", "    {@ inline @}", "ZZZ"]
        expected = ["AAA", "Partial for content only!", "NOARGS", "", "<hr />", "ZZZ"]
        result = self.preprocessor.run(lines)
        self.assertEqual(expected, result)

    def test_recursion_limit(self):
        """It should not get stuck"""
        lines = ["AAA", "@@@ infinite_block", "ZZZ"]
        expected = ["AAA", "@@@ infinite_block", "Y", "Y", "Y", "Y", "Y", "Y", "Y", "Y", "Y",  "ZZZ"]
        result = self.preprocessor.run(lines)
        self.assertEqual(expected, result)

    def test_recursion_limit_config(self):
        """Recursion limit can be changed"""
        config = {}
        config['path'] = os.path.join(os.path.dirname(os.path.realpath(__file__)), "files")
        config['prefix'] = '@'
        config['recursion_max'] = 3
        md = markdown.Markdown()
        other_preprocessor = SmidgensPreprocessor(md, config)

        lines = ["AAA", "@@@ infinite_block", "ZZZ"]
        expected = ["AAA", "@@@ infinite_block", "Y", "Y", "Y", "ZZZ"]
        result = other_preprocessor.run(lines)
        self.assertEqual(expected, result)

    def test_two_adjacent_blocks(self):
        """Handles two adjacent blocks"""
        lines = ["AAA", "@@@ echo_content", "    One", "", "@@@ echo_content", "    Two", "", "ZZZ"]
        expected = ["AAA", "Partial for content only!", "One", "", "<hr />", "", "Partial for content only!", "Two", "", "<hr />", "", "ZZZ"]
        result = self.preprocessor.run(lines)
        self.assertEqual(expected, result)

    def test_two_adjacent_blocks_in_recursion(self):
        """Handles two adjacent blocks in recursion"""
        lines = ["AAA", "@@@ adjacent_recursive_block", "ZZZ"]
        expected = ["AAA", "These should be processed separately", "", "Blocks...", "", "Foo", "", "<hr />", "", "Blocks...", "", "Bar", "", "<hr />", "ZZZ"]
        result = self.preprocessor.run(lines)
        self.assertEqual(expected, result)


if __name__ == '__main__':
        unittest.main()
