# mdx_smidgens -- Python Markdown extension to include pre-made snippets.
# Copyright (C) 2019-2024 Daniel Fancsali <fancsali@gmail.com>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License version 3
# as published by the Free Software Foundation

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import unittest
from mdx_smidgens import SmidgensFormatter


class TestFormatter(unittest.TestCase):

    def setUp(self):
        self.formatter = SmidgensFormatter()

    def test_replaces_one(self):
        """Replaces one placeholder"""
        arg_dict = {"1": "X"}
        pattern = "A @1 B"
        expected = "A X B"
        result = self.formatter.format(pattern, arg_dict)
        self.assertEqual(expected, result)

    def test_replaces_one_without_spaces(self):
        """Replaces one placeholder"""
        arg_dict = {"1": "X"}
        pattern = "A@1B"
        expected = "AXB"
        result = self.formatter.format(pattern, arg_dict)
        self.assertEqual(expected, result)

    def test_replaces_one_leaves_others(self):
        """Replaces one placeholder, and removes not-found ones"""
        arg_dict = {"1": "X"}
        pattern = "A @1 @2 B"
        expected = "A X  B"
        result = self.formatter.format(pattern, arg_dict)
        self.assertEqual(expected, result)

    def test_replaces_at_beginning(self):
        """Replaces one placeholder at the beginning of the line"""
        arg_dict = {"1": "X"}
        pattern = "@1 Z"
        expected = "X Z"
        result = self.formatter.format(pattern, arg_dict)
        self.assertEqual(expected, result)

    def test_replaces_at_end(self):
        """Replaces one placeholder at the beginning of the line"""
        arg_dict = {"1": "X"}
        pattern = "A @1"
        expected = "A X"
        result = self.formatter.format(pattern, arg_dict)
        self.assertEqual(expected, result)

    def test_replaces_more(self):
        """Replaces more than one placeholder"""
        arg_dict = {"1": "X", "2": "Y"}
        pattern = "A @1 @2 B"
        expected = "A X Y B"
        result = self.formatter.format(pattern, arg_dict)
        self.assertEqual(expected, result)

    def test_replaces_one_longer_string(self):
        """Replaces one placeholder with a non-one long string"""
        arg_dict = {"1": "XYZ"}
        pattern = "A @1 B"
        expected = "A XYZ B"
        result = self.formatter.format(pattern, arg_dict)
        self.assertEqual(expected, result)

    def test_replaces_zero(self):
        """Replaces a '0' placeholder"""
        arg_dict = {"0": "one two three"}
        pattern = "Zero: @0"
        expected = "Zero: one two three"
        result = self.formatter.format(pattern, arg_dict)
        self.assertEqual(expected, result)

    def test_handles_double_prefix(self):
        """A double prefix should not act as an escape"""
        arg_dict = {"1": "foo"}
        pattern = "Well: @1 @@1"
        expected = "Well: foo @foo"
        result = self.formatter.format(pattern, arg_dict)
        self.assertEqual(expected, result)

    def test_handles_triple_prefix(self):
        """A triple prefix would be left alone for further processing even with a number attached"""
        arg_dict = {"1": "foo"}
        pattern = "Well: @1 @@@1"
        expected = "Well: foo @@@1"
        result = self.formatter.format(pattern, arg_dict)
        self.assertEqual(expected, result)

    def test_handles_backslash(self):
        """A backslash should act as an escape"""
        arg_dict = {"1": "foo"}
        pattern = "Well: @1 \@1"
        expected = "Well: foo \@1" # Escapes are handled in SmidgensPostProcessor
        result = self.formatter.format(pattern, arg_dict)
        self.assertEqual(expected, result)

    def test_handles_triple_prefix_nonumber(self):
        """A triple prefix would be left alone on it's own"""
        arg_dict = {"1": "foo"}
        pattern = "Well: @1 @@@ 1"
        expected = "Well: foo @@@ 1"
        result = self.formatter.format(pattern, arg_dict)
        self.assertEqual(expected, result)

    def test_handles_backslash_and_placeholder(self):
        """Combining backslash and a placeholder"""
        arg_dict = {"1": "foo"}
        pattern = "Well: @1 \@@1"
        expected = "Well: foo \@foo" # Escaping is handled in SmidgensPostprocessor
        result = self.formatter.format(pattern, arg_dict)
        self.assertEqual(expected, result)

    def test_handles_backslash_and_triple(self):
        """Backslash some part of a triplet"""
        arg_dict = {"1": "foo"}
        pattern = "Well: @1 \@@@ 1"
        expected = "Well: foo \@@@ 1" # Escaping is handled in SmidgenPostrpocessor
        result = self.formatter.format(pattern, arg_dict)
        self.assertEqual(expected, result)

    def test_handles_triplets_both_escaped_and_unescaped(self):
        """One can include triple @'s, both ways"""
        arg_dict = {"1": "foo"}
        pattern = "Well: @1 \@\@\@ 1 // @@@ 1"
        expected = "Well: foo \@\@\@ 1 // @@@ 1"
        result = self.formatter.format(pattern, arg_dict)
        self.assertEqual(expected, result)

    def test_numerical_keys(self):
        """It should find string and numerical keys too"""
        arg_dict = {"1": "foo", "2": "bar"}
        pattern = "A @1 @2 B"
        expected = "A foo bar B"
        result = self.formatter.format(pattern, arg_dict)
        self.assertEqual(expected, result)

    def test_replaces_underscore(self):
        pass

    def test_prefix_can_be_customised(self):
        """The prefix can be chosen when instantiating the class"""
        other_formatter = SmidgensFormatter("%")
        arg_dict = {"1": "X"}
        pattern = "A @1 %1 B"
        expected = "A @1 X B"
        result = other_formatter.format(pattern, arg_dict)
        self.assertEqual(expected, result)

    def test_prefix_will_be_escaped(self):
        """The prefix can be chosen to be a regex special character"""
        other_formatter = SmidgensFormatter("$")
        arg_dict = {"1": "X"}
        pattern = "A @1 $1 B"
        expected = "A @1 X B"
        result = other_formatter.format(pattern, arg_dict)
        self.assertEqual(expected, result)


if __name__ == '__main__':
        unittest.main()
