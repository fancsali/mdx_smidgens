# mdx_smidgens

Python Markdown extension to include pre-made snippets.

## Inspiration

This extension was mainly inspired by Hugo shortcodes. For the basic syntax inspiration has been drawn from the official [admonition](https://python-markdown.github.io/extensions/admonition/) Python Markdown plugin.

## Usage

The extension can be used to include partial *MD* files into documents, allowing one to create re-usable snippets for recurring text.

Templates can be included as independent blocks or as inline text snippets.

### Block syntax

It will parse blocks of the format:

```
@@@ path/name arg1 "arg 2" arg3
    Some block content.

    Some more.

Not part of the content anymore
```

### Inline syntax

Includes can also be invoked inline:

```
Some inline text, {@ path/name arg1 "arg 2" arg3 @}
```

### Templates

- Both forms will try to load the file `path/_name.md` below the configured partial path. (See *Configuration*)
- In those partials one can use variable substitutions:
    - Within templates the arguments can be accessed by typing `@n` where `n` is the index of the particular argument, starting from one. The variable `@0` will return the whole argument line, but without the partial name.
    - If invoked by the block syntax, all the indented blocks, that follow the initial line will be moved one indent level up, and then collected into the special variable `@_`. (**NOTE**: In accordance with the rest of python-markdown, multiple consecutive blocks, separated by empty lines are all considered as one.)
- Lastly, after the substitutions are made, the resulting text is ran through markdown, so one can use all the usual syntax in the partials.

### Recursive invocation

- Templates can invoke the plugin recursively, that is a template `path/_first.md` can contain something like:

```
@@@ path/second
```

...to include yet another smidgen.

- However 
    - Nesting **inline** invocations is currently undefined, as regular-expressions cannot handle nesting by defintion. So `{@ foo {@ bar @} {@ baz @} @}` will not work!
    - Invoking block embedding is by defintion a line-wise operation, so embedding a block inside an inline invocation is undefined too. So `{@ spam @@@ ham two three @}` won't really do anything meaningful (besides ignoring the three `@`-signs).
- **NOTE**: There's a limit for the depth of the recursion, which is set to be **9** by default; but can be changed in the config.

### Escaping

Both variable substitution in templates and invocations of the embedding logic can be prevented by escaping with `\`:

- `\@@@ foo bar baz` will not try to embed the `_foo.md` template;
- All this will be ignored: `\{@ foo @}`, `{\@ bar @}` and `\{\@ baz @}`;
- In templates `\@n` and `\@_` will not be replaced.

## Configuration

- There's one single configuration key, that is mandatory:
    - `path`, which contains the absolute path to the **root** directory containing the partials.

- There are some optional ones too:
    - `recursion_max`, enables the user to adjust the depth limit for recursive invocations (default: `9`)
    - `prefix`, allows one to change the variable prefix used in templates from `@` to some other character

## Contributing

- Set up virtual environment: `python3 -m venv venv`
- Install required packages
    - markdown
- Don't forget to run the tests
    - `python -m unittest tests/*.py`
