# mdx_smidgens -- Python Markdown extension to include pre-made snippets.
# Copyright (C) 2019-2024 Daniel Fancsali <fancsali@gmail.com>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License version 3
# as published by the Free Software Foundation

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from setuptools import setup

setup(
    name='markdown-smidgens',
    version='0.1',
    description='An easy to use snippets extension for Python Markdown',
    author='Daniel Fancsali',
    author_email='fancsali@gmail.com',
    license='AGPL-3.0-only',
    license_files = ('LICENSE',),
    install_requires=['markdown'],
    py_modules=['mdx_smidgens']
)
