# mdx_smidgens -- Python Markdown extension to include pre-made snippets.
# Copyright (C) 2019-2024 Daniel Fancsali <fancsali@gmail.com>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License version 3
# as published by the Free Software Foundation

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from markdown.extensions import Extension
from markdown.preprocessors import Preprocessor
from markdown.postprocessors import Postprocessor
import re
import shlex
import os

class SmidgensFormatter:
    def __init__(self, prefix='@'):
        pre = re.escape(prefix)
        pattern = fr"(?P<escaped>\\{pre})|(?P<tri>@@@)|{pre}(?P<key>[_0-9])"
        self.pattern = re.compile(pattern)

    def format(self, template, args={}):
        # Make sure all the keys are strings
        self.args_dict = { k.__str__(): v for k,v in args.items()}
        return self.pattern.sub(self.get_sub, template)

    def get_sub(self, mo):
        # If captured an escaped prefix, return a prefix
        if mo.group('escaped') is not None:
            return mo.group('escaped')
        # If we've captured a triplet; let that be processed later
        if mo.group('tri') is not None:
            return mo.group('tri')
        # Otherwise try to look up the key
        if mo.group('key') is not None and mo.group('key') in self.args_dict:
            return self.args_dict[mo.group('key')]

        # If nothing found at all, return an empty string
        return ""


class SmidgensPreprocessor(Preprocessor):
    BLOCK_RE = r'^(?<!\\)@@@[ \t]*(?P<name>[\w/-]+)([ \t]+(?P<args>.*?))?[ \t]*$'
    INLINE_RE = r'(?<!\\)\{(?<!\\)@[ \t]*(?P<name>[\w/-]+)([ \t]+(?P<args>.*?))?[ \t]*@\}'

    def __init__(self, md=None, config={}):
        self.md = md
        self.path = config['path']
        self.formatter = SmidgensFormatter(config['prefix'])
        self.recursion_max = config['recursion_max']

        self.compiled_block_re = re.compile(SmidgensPreprocessor.BLOCK_RE)
        self.compiled_inline_re = re.compile(SmidgensPreprocessor.INLINE_RE)

        super().__init__(md)

    def run(self, lines):
        new_lines = []

        changed = None # We don't know yet, but the while needs a condition
        depth = 0

        while depth < self.recursion_max and (changed is None or changed is True):
            i = 0
            changed = False
            while i < len(lines):
                m = self.compiled_block_re.match(lines[i])
                if m is not None:
                    content = []
                    #Capture tabbed blocks
                    while i+1 < len(lines) and lines[i+1].startswith(' ' * self.md.tab_length):
                        content.append(lines[i+1][self.md.tab_length:])
                        i += 1
                        # Collect all 'empty' lines, assuming there's another block ahead
                        j = 0
                        while i+1+j < len(lines) and (len(lines[i+1+j]) == 0 or lines[i+1+j].isspace()):
                            j += 1
                        # So, is there a block ahead? If so, add the empty lines, and let the collection of blocks continue; just go ahead otherwise
                        if i+1+j < len(lines) and lines[i+1+j].startswith(' ' * self.md.tab_length):
                            content.extend(lines[i+1:i+1+j])
                            i += j
                    new_text = self.__apply(m, "\n".join(content))
                    new_lines.extend(new_text.splitlines())
                    changed = True
                elif lines[i] == "":
                    # Preserving empty lines
                    new_lines.append("")
                else:
                    new_text, n = self.compiled_inline_re.subn(self.__apply, lines[i])
                    new_lines.extend(new_text.splitlines())
                    if n > 0:
                        changed = True

                i += 1

            if changed:
                lines = new_lines
                new_lines = []

            depth += 1 # Finished one pass

        return lines

    def __parse_args(self, smidgen_args):
        if smidgen_args is None:
            return dict()

        parsed_args = dict()
        parsed_args[0] = smidgen_args
        split_args = shlex.split(smidgen_args)
        parsed_args.update(zip(range(1, len(split_args)+1), split_args))

        return parsed_args

    def __get_snippet(self, template_name):
        template_path_components = template_name.split("/")
        template_path_components[-1] = "_" + template_path_components[-1] + ".md"
        template_filename = os.path.join(self.path, *template_path_components)
        template_file = open(template_filename, "r")
        template_text = template_file.read()
        template_file.close()

        return template_text.rstrip("\n")

    def __apply(self, m, content=None):
        # Find the appropriate template file, and load it
        snippet = self.__get_snippet(m.group('name'))

        args = self.__parse_args(m.group('args'))

        args['_'] = content  # Inject the full text content

        # Process the template and the arguments
        return self.formatter.format(snippet, args)

class SmidgensPostprocessor(Postprocessor):
    def __init__(self, md=None, config={}):
        self.md = md
        self.prefix = config['prefix']

    def run(self, text):
        text = text.replace('\\' + self.prefix, self.prefix)
        return text

class SmidgensExtension(Extension):
    def __init__(self, **kwargs):
        self.config = {
                       'path': ["", "Directory root containing partials"],
                       'recursion_max':[9, "Maximum recursive includes allowed"],
                       'prefix': ["@", "Prefix for argument references"]
                      }

        super().__init__(**kwargs)

    def extendMarkdown(self, md):
        md.registerExtension(self)

        if (self.getConfig('path') == ""):
            raise ValueError("The 'path' must be specified")

        if (self.getConfig('prefix') == ""):
            raise ValueError("The 'prefix' cannot be empty")

        md.preprocessors.register(SmidgensPreprocessor(md, self.getConfigs()), 'smidgens', 50)
        md.postprocessors.register(SmidgensPostprocessor(md, self.getConfigs()), 'smidgens', 50)


def makeExtension(*args, **kwargs):
    return SmidgensExtension(*args, **kwargs)
